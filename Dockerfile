# Sources:
# - https://docs.docker.com/develop/develop-images/multistage-build/
# - https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/

FROM ubuntu:20.04 AS updated_base_image
# Update the image and install dependencies
ENV TZ=Europe/Berlin
ARG DEBIAN_FRONTEND=noninteractive
ARG APT_FLAGS="--assume-yes --quiet --quiet --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages"
RUN apt-get $APT_FLAGS update &&\
    apt-get install $APT_FLAGS apt-utils make &&\
    apt-get clean

FROM updated_base_image AS npm_build_image
ARG APT_FLAGS="--assume-yes --quiet --quiet --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages"
ENV TZ=Europe/Berlin
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get $APT_FLAGS update &&\
    apt-get install $APT_FLAGS apt-utils &&\
    apt-get install $APT_FLAGS npm &&\
    apt-get clean

FROM npm_build_image as npm_build_stage
WORKDIR /usr/src/app
COPY ${PWD}/frontend /usr/src/app/frontend/

# configure npm
# https://docs.npmjs.com/cli/v7/using-npm/config
ENV npm_config_cache="/tmp/npmcache"
ENV npm_config_prefix="/usr/src/app/frontend"

# install the production dependencies
RUN npm \
    #--production \
    --prefix=${npm_config_prefix} \
    install &&\
    rm --recursive --force "$npm_config_cache"

# build the css
RUN npm \
    --production \
    --prefix=${npm_config_prefix} \
    run css-build &&\
    rm --recursive --force "$npm_config_cache"

# build the production frontend
RUN npm \
    --production \
    --prefix=${npm_config_prefix} \
    run build &&\
    rm --recursive --force "$npm_config_cache"

FROM updated_base_image AS nginx_image
# install nginx
# https://www.pluralsight.com/guides/packaging-a-django-app-using-docker-nginx-and-gunicorn
ARG APT_FLAGS="--assume-yes --quiet --quiet --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages"
ENV TZ=Europe/Berlin
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get $APT_FLAGS update &&\
    apt-get install $APT_FLAGS apt-utils &&\
    apt-get install $APT_FLAGS nginx-core &&\
    apt-get clean

# configure nginx
# https://www.pluralsight.com/guides/packaging-a-django-app-using-docker-nginx-and-gunicorn
COPY ${PWD}/deployment/nginx.conf /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default

# Add static files to nginx
COPY --from=npm_build_stage /usr/src/app/backend/static /usr/src/app/backend/static

# Define working directory.
WORKDIR /etc/nginx

# Define default command.
CMD ["nginx"]

# Expose ports.
EXPOSE 80
EXPOSE 443

FROM updated_base_image AS python_image
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ARG APT_FLAGS="--assume-yes --quiet --quiet --allow-unauthenticated --allow-downgrades --allow-remove-essential --allow-change-held-packages"
ENV TZ=Europe/Berlin
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get $APT_FLAGS update &&\
    apt-get install $APT_FLAGS apt-utils &&\
    apt-get install $APT_FLAGS python3-pip &&\
    apt-get clean

WORKDIR /usr/src/app
# add minimum python requirements
COPY ${PWD}/requirements.txt .
# install poetry
RUN pip install \
            --requirement requirements.txt
# install the django
COPY ${PWD}/pyproject.toml .
COPY ${PWD}/poetry.lock .
RUN poetry install

FROM python_image AS run_image
# install the django backend
WORKDIR /usr/src/app
COPY GNUmakefile /usr/src/app/GNUmakefile
COPY backend /usr/src/app/backend
COPY templates /usr/src/app/templates
COPY --from=npm_build_stage /usr/src/app/frontend/webpack-stats.json /usr/src/app/frontend/

RUN make recreate_database

CMD ["poetry","run","backend/manage.py","runserver","0.0.0.0:8000"]

EXPOSE 8000
