# Scrum Poker

## Docker Container

Der Docker Container kann mit docker-compose erstellt und gestartet werden:

```sh
$ docker-compose up
```

Es muss allerdings die (per moodle bereitgestellte) .env.secrets Datei im root Ordner abgelegt werden, sodass der Container mit social login baut.

Bei Fragen und Problemen kannst du uns gerne eine E-Mail schreiben.

## Features

Die Hauptfunktion unseres Programmes ist das man ohne Login schnell kollaborative Räume erstellen kann in denen Scrum Poker Abstimmungen gemacht werden können.

Die Räume unterstützen:
- Echtzeit Abstimmungen von mehreren Usern
- Auswertung der Abstimmungen (average, highest vote, vote count etc.)
- Mehrere benannte Abstimmungen anzulegen (z.b. für jedes Ticket)
- Umfassendes Berechtigungsystem für jede einzelne Aktion
- Automatische Zuweisung der Admin Gruppenrolle (erster User im Raum bzw. bei Verlassen der User am längsten im Raum)
- Diverse weitere Features z.B. Umbennung von Namen, Kicken von Usern etc.
- Einladen anderer über E-Mail oder Link kopieren

Für eine einfache Benutzung werden auf der Startseite Raumnamen automatisch generiert sowie die letzen Räume angezeigt (per local storage gespeichert). Außerdem wird für einen Raum auch der Username in diesem Raum per cache gespeichert und automatisch gesetzt.

Per social login kann auch ein persistenter Account erstellt werden. Dabei werden diverse Provider z.B. Gitlab, Github, Steam unterstützt. Eingeloggte User können sogenannte "Gruppen" erstellen. Dieses sind im wesentlichen persistente Räume. Alle angemeldeten User werden im Raum dauerhaft gespeichert / angezeigt und bekommen stattdessen einen Online Status. Zusätzlich werden auch alle Abstimmungen dauerhaft gespeichert.

Zuletzt in der Umsetzung war noch ein Feature um Räumen nur per speziellen Link beitreten zu können (automatisch generierbar). Das Feature hat es jedoch aus Zeitgründen nicht mehr in den master branch geschafft (Deswegen auch die noch leere Settings "Allgemein" Seite aktuell in einem Raum)

## Dependencies

### Backend

- python 3.8
- poetry

Install other dependencies with poetry

```sh
$ poetry install
```

### Frontend

- npm

Install other dependencies with npm

```sh
$ cd frontend & npm install
```

## Development Server

Start the development server with:

```sh
$ make run_backend_dev
```

# Connection to OAUTH providers

Add the necessary keys in `backend/scrum_poker_site/local_secrets.py`.

# Configuration of the docker images

Configuration is done via the following environment variables:

```python
DJANGO_DEBUG_LEVEL: The verbosity of the loggers
        Defaults to 'WARN'
DJANGO_DEBUG_FILE: File to log debug information to
        Defaults to 'BASE_DIR + debug.log'
DJANGO_SQLITE_PATH: Path for the user database.
        Defaults to 'BASE_DIR + db.sqlite3'
```

The following environment variables are directly used for the django variables without the  `DJANGO_`-prefix.

```python
DJANGO_DEBUG: Defaults to True
DJANGO_SECRET_KEY: The key to use for the 'SECRET_KEY' setting
DJANGO_ALLOWED_HOSTS: Defaults to ['localhost','127.0.0.1','::1']
D
```
