import os

from django.core.management.base import BaseCommand, CommandError

# https://docs.djangoproject.com/en/3.2/topics/logging/#topic-logging-parts-loggers
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Drop and recreate the database"

    def handle(self, *args, **options):
        try:
            os.remove("db.sqlite3")
        except:
            logger.warning("No database found")
