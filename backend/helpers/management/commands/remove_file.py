import os

from django.core.management.base import BaseCommand, CommandError

# https://docs.djangoproject.com/en/3.2/topics/logging/#topic-logging-parts-loggers
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Remove file"

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'filename', type=str, nargs=1, help='file to delete'
        )

    def handle(self, *args, **options):
        for f in options['filename']:
            try:
                os.remove(f)
            except:
                logger.warning("Could not remove " + f)
