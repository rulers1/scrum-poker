"""
Source:
    https://stackoverflow.com/questions/42497099/can-manage-py-runserver-execute-npm-scripts
"""
import os

# This app is only enabled in debug mode
import subprocess  # nosec
import threading

from django.contrib.staticfiles.management.commands.runserver import (
    Command as StaticFilesRunserverCommand,
)
from django.utils.autoreload import DJANGO_AUTORELOAD_ENV


class Command(StaticFilesRunserverCommand):
    """This command removes the need for two terminal windows when running runserver."""

    help = (
        "Starts a lightweight Web server for development and also serves static files. "
        "Also runs a webpack build worker in another thread."
    )

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            "--webpack-command",
            dest="wp_command",
            default="npm --prefix frontend/ run build --watch",
            help=
            "This webpack build command will be run in another thread (should probably have --watch).",
        )
        parser.add_argument(
            "--webpack-quiet",
            action="store_true",
            dest="wp_quiet",
            default=False,
            help="Suppress the output of the webpack build command.",
        )

    def run(self, **options):
        """Run the server with webpack in the background."""
        if os.environ.get(DJANGO_AUTORELOAD_ENV) != "true":
            self.stdout.write("Starting webpack build thread.")
            quiet = options["wp_quiet"]
            command = options["wp_command"]
            kwargs = {
                "shell": True
            }
            if quiet:
                # if --quiet, suppress webpack command's output:
                kwargs.update({
                    "stdin": subprocess.PIPE, "stdout": subprocess.PIPE
                })
            wp_thread = threading.Thread(
                target=subprocess.run, args=(command, ), kwargs=kwargs
            )
            wp_thread.start()
        super(Command, self).run(**options)
