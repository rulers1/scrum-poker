from .utils import get_socialaccount_providers_dict, get_social_accounts_list
from index_app.models import RoomUser


def provide_social_account_list(request):
    socialaccount_providers_list = get_socialaccount_providers_dict(
        request=request
    )
    socialaccount_list = get_social_accounts_list(request=request)

    return {
        "socialaccount_providers_list": socialaccount_providers_list,
        "socialaccount_list": socialaccount_list,
    }


def provide_teams(request):
    teams = []
    if request.user.is_authenticated:
        for roomuser in RoomUser.objects.filter(user=request.user):
            teams.append(roomuser.presence.room.roomCode)
    return {
        "teams": teams
    }
