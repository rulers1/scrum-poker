from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.http import HttpResponse, HttpResponseForbidden

from django.contrib.auth import (logout as django_logout)
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from allauth.socialaccount.models import SocialAccount

# https://docs.djangoproject.com/en/3.2/topics/logging/
# import the logging library
import logging

import json

# Get an instance of a logger
logger = logging.getLogger(__name__)


# Create your views here.
def login(request):
    return register(request)


def register(request):
    return render(request, "login.html", {})


@login_required
def delete(request):
    """
    Delete the user account
    """
    logger.error({"request": request})
    if request.method == 'POST':
        if request.user.is_authenticated:
            # Source https://stackoverflow.com/a/63473816
            user = request.user
            # Logout before we delete. This will make request.user
            # unavailable (or actually, it points to AnonymousUser).
            logout(request)
            # Delete user (and any associated ForeignKeys, according to
            # on_delete parameters).
            user.delete()

    if "HTTP_REFERER" in request.META:
        response = redirect(request.META["HTTP_REFERER"])
    else:
        response = redirect("/")

    return response


@login_required
def disconnect(request):
    """
    Delete the connection between a user account and a socialaccount
    """
    if request.method == 'POST':
        user = request.user
        disconnect_provider = request.POST["disconnectProvider"]
        disconnect_account = request.POST["disconnectAccount"]
        if user.is_authenticated:
            socialaccount_set = user.socialaccount_set
            accounts = SocialAccount.objects.filter(user=request.user)

            # Handling if only one socialaccount is left
            if len(accounts) == 1:
                # Delete if last connected socialaccount
                autoDelete = True
                if autoDelete:
                    # Source https://stackoverflow.com/a/63473816
                    user = request.user
                    # Logout before we delete. This will make request.user
                    # unavailable (or actually, it points to AnonymousUser).
                    logout(request)
                    # Delete user (and any associated ForeignKeys, according to
                    # on_delete parameters).
                    user.delete()
                    logger.warn("Deleted user")
                    messages.success(request, 'Account successfully deleted')
                else:
                    logger.warn("Will not remove last account")

                if request.META.HTTP_REFERER:
                    response = redirect(request.META.HTTP_REFERER)
                else:
                    response = redirect("/")
                return response

            for account in accounts:
                provider_account = account.get_provider_account()
                logger.debug(provider_account)
                provider_account_profile_url = provider_account.get_profile_url(
                )
                logger.debug(provider_account_profile_url)

                if disconnect_account == provider_account_profile_url:
                    logger.info(
                        "Removed account " + provider_account_profile_url
                    )
                    account.delete()

    if request.META.HTTP_REFERER:
        response = redirect(request.META.HTTP_REFERER)
    else:
        response = redirect("/")
    return response


@login_required
def profile(request):
    return render(request, "profile.html", {})


@login_required
def changeUserName(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        username = data["userName"]
        if User.objects.filter(username=username).exists():
            return HttpResponseForbidden()

        owner = request.user
        owner.username = username
        owner.save()

        return HttpResponse()
    return HttpResponse()


@login_required
def checkUserNameAvailable(request):
    if request.method == 'POST':
        data = json.loads(request.body.decode("utf-8"))
        username = data["userName"]
        if User.objects.filter(username=username
                               ).exclude(username=request.user.username
                                         ).exists():
            return HttpResponseForbidden()
        return HttpResponse()
    return HttpResponse()


@login_required
def logout(request):
    if request.method == 'POST':
        django_logout(request)
    response = redirect("/")
    return response
