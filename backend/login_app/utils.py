from allauth.socialaccount import providers
from allauth.socialaccount.templatetags.socialaccount import ProviderLoginURLNode,get_social_accounts

# https://docs.djangoproject.com/en/3.2/topics/logging/
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


def get_management_urls():
    return {
        "logoutURL": accounts_logout
    }


def get_social_accounts_list(request):
    """This returns all user accounts in a json compatible format
    """
    accounts = {}
    if request.user.is_authenticated:
        socialaccount_set = request.user.socialaccount_set
        providers = []
        for base_account in socialaccount_set.all().iterator():
            providers = accounts.setdefault(base_account.provider, [])
            providers.append({
                "accountProfileUrl":
                    base_account.get_provider_account().get_profile_url(),
                "accountAvatarUrl":
                    base_account.get_provider_account().get_avatar_url(),
                "id": base_account.id,
                "providerName": base_account.provider
            })

    return accounts


def get_socialaccount_providers_dict(request):
    """This returns the data needed to create the login page
    """
    account_providers = providers.registry.get_list()
    # Defaults

    socialaccount_providers = []
    for provider in account_providers:
        if provider.id == "openid":
            for brand in provider.get_brands():
                socialaccount_providers.append({
                    "providerName": brand.get('name'),
                    "providerClass":
                        " ".join([
                            "socialaccount_provider",
                            provider.id,
                            brand.get('id')
                        ]),
                    "providerLoginUrl":
                        provider.get_login_url(
                            request=request, openid=brand.get('openid_url')
                        )
                })

        socialaccount_providers.append({
            "providerName": provider.name,
            "providerClass": " ".join(["socialaccount_provider", provider.id]),
            "providerLoginUrl": provider.get_login_url(request=request)
        })

    return socialaccount_providers
