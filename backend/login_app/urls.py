from django.urls import path, include, re_path, reverse
from django.views.generic import TemplateView, RedirectView
from . import views

OVERRIDE_URLS = [

    # Source: https://github.com/pennersr/django-allauth/issues/345#issuecomment-807600317
    # These URLs shadow django-allauth URLs to shut them down:
    path('password/change/', RedirectView.as_view(url='/')),
    path('password/set/', RedirectView.as_view(url='/')),
    path('password/reset/', RedirectView.as_view(url='/')),
    path('password/reset/done/', RedirectView.as_view(url='/')),
    re_path(
        '^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$',
        RedirectView.as_view(url='/')
    ),
    path('password/reset/key/done/', RedirectView.as_view(url='/')),
    path('email/', RedirectView.as_view(url='/')),
    path('confirm-email/', RedirectView.as_view(url='/')),
    re_path(
        '^confirm-email/(?P<key>[-:\\w]+)/$', RedirectView.as_view(url='/')
    ),
]

urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('profile/', views.profile, name='profile'),
    path(
        'profile/changeUserName', views.changeUserName, name='changeUserName'
    ),
    path(
        'profile/checkUserNameAvailable',
        views.checkUserNameAvailable,
        name='checkUserNameAvailable'
    ),
    path('register/', views.register, name='login'),
    path('accounts/login/', RedirectView.as_view(url="/login")),
    path('accounts/signup/', RedirectView.as_view(url="/login")),
    path('accounts/disconnect/', views.disconnect, name='disconnect'),
    path('accounts/delete/', views.delete, name='delete'),
    path('accounts/', include('allauth.urls')),
    path('accounts/', include(OVERRIDE_URLS)),
]
