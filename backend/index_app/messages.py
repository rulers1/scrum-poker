import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.dispatch import receiver

channel_layer = get_channel_layer()


def sendOwnUserMessage(user, changeCookie, checkCookieCondition):
    message = {
        "type": "own_user",
        "payload": {
            "user": user.asDict(),
            "changeCookie": changeCookie,
            "checkCookieCondition": checkCookieCondition
        }
    }
    sendMessage(user, message)


def sendVotings(user, room):
    from .models import RoomVoting

    message = {
        "type": "votings",
        "payload": {
            "votings": RoomVoting.objects.getVotings(room)
        }
    }
    sendMessage(user, message)


def sendMessage(user, message):
    async_to_sync(
        channel_layer.send
    )(user.channelName, {
        "type": "message", "message": json.dumps(message)
    })