import json

from django import dispatch

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.dispatch import receiver
from django.db.models.signals import post_delete
from .models import Presence

from .messages import sendOwnUserMessage, sendVotings

channel_layer = get_channel_layer()

presenceChanged = dispatch.Signal(providing_args=["room"])
ownPresenceChanged = dispatch.Signal(providing_args=["user", "changeCookie","checkCookieCondition"])
presenceAdded = dispatch.Signal(providing_args=["room", "added"])
presenceRemoved = dispatch.Signal(providing_args=["room", "removed"])

votingChanged = dispatch.Signal(providing_args=["room", "votings"])


@receiver(presenceAdded)
def onPresenceAdded(sender, room, added, **kwargs):
    """
    Check if the new added user is the first one (becomes admin) and notify that user
    """
    added.user.addGroup("user")

    if (not room.atLeastOneAdmin()):
        added.user.addGroup("admin")

    sendOwnUserMessage(added.user, False,True)
    sendVotings(added.user, room)
    broadcastPresence(room)


@receiver(presenceRemoved)
def onPresenceRemoved(sender, room, removed, **kwargs):
    """
    Assign a new admin if the removed user was the last admin in the room
    """

    if (room.atLeastOneAdmin()):
        return

    presence = room.presence_set.first()
    if (presence):
        presence.user.addGroup("admin")
        sendOwnUserMessage(presence.user, False,True)

@receiver(post_delete, sender=Presence)
def autoDeleteUserWithPresence(sender, instance, **kwargs):
    instance.user.delete()



@receiver(ownPresenceChanged)
def onOwnPresenceChanged(user, changeCookie, checkCookieCondition, **kwargs):
    sendOwnUserMessage(user, changeCookie,checkCookieCondition)


@receiver(presenceChanged)
def broadcastPresence(room, **kwargs):
    """
    Broadcast the new list of present users to the room.
    """
    print("Broadcast!")

    message = {
        "type": "presence",
        "payload": {
            "channel_name": room.roomChannelName,
            "users": room.getUserPresences(),
        }
    }

    async_to_sync(channel_layer.group_send)(
        room.roomChannelName, {
            "type": "message", "message": json.dumps(message)
        }
    )


@receiver(votingChanged)
def broadcastVotings(room, votings, **kwargs):
    message = {
        "type": "votings", "payload": {
            "votings": votings
        }
    }

    async_to_sync(channel_layer.group_send)(
        room.roomChannelName, {
            "type": "message", "message": json.dumps(message)
        }
    )