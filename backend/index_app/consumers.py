import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from .models import Room, RoomUser, UserVote, UserVoteManager, RoomVoting, Permission

from random import randrange
import logging
import re


class CommunicationConsumer(AsyncWebsocketConsumer):
    logger = logging.getLogger(__name__)

    async def connect(self):
        self.persistent = False
        if (self.scope['url_route']['kwargs']['room_team'] == "team"):
            self.persistent = True

        roomCode = self.scope['url_route']['kwargs']['room_code']
        roomname = f"{self.scope['url_route']['kwargs']['room_team']}_{roomCode}"
        self.room = await self.getRoom(roomCode, roomname)

        self.user = await self.createUser(
            f"AnonymousUser {randrange(10000)}"  # nosec
        )

        await self.accept()
        self.logger.info("Connection established: " + self.user.channelName)

        await self.addUserToRoom(self.user)
        await self.sendPermissions()

    async def disconnect(self, close_code):
        await self.disconnectUserFromRoom(self.user)

    @database_sync_to_async
    def createUser(self, userName):
        user = self.room.getUserByLoginAndUpdate(
            self.scope["user"], self.channel_name, self.persistent
        )
        if not user:
            user = RoomUser.objects.createUser(
                self.channel_name, userName, self.scope["user"]
            )
        return user

    @database_sync_to_async
    def getRoom(self, roomCode, roomName):
        room = Room.objects.get(roomCode, roomName, self.persistent)
        RoomVoting.objects.addDefaultVoting(room)
        return room

    @database_sync_to_async
    def addUserToRoom(self, user):
        self.room.addPresence(user)

    async def sendPermissions(self):
        await self.sendMessage(
            self.user.channelName,
            "all_permissions",
            payload={"permissions": await self.getPermissions()}
        )

    @database_sync_to_async
    def getPermissions(self):
        return [{
            "name": permission.name
        } for permission in Permission.objects.all()]

    @database_sync_to_async
    def removeUserFromRoom(self, user):
        self.room.removePresence(user)

    @database_sync_to_async
    def disconnectUserFromRoom(self, user):
        self.room.disconnectPresence(user)

    async def receive(self, text_data):
        response = json.loads(text_data)

        event = response["type"]
        self.logger.info("Event: " + event)

        if event == "kick_user":
            await self.recieveKickUserMessage(response["payload"])
        elif event == "change_username":
            await self.changeUsernameMessage(response["payload"])
        elif event == "vote":
            await self.voteMessage(response["payload"])
        elif event == "voting_name_changed":
            await self.votingNameChangedMessage(response["payload"])
        elif event == "show_votes":
            await self.showVotesMessage(response["payload"])
        elif event == "next_voting":
            await self.nextVoting(response["payload"])
        elif event == "add_voting":
            await self.addVotingMessage()
        elif event == "remove_voting":
            await self.removeVotingMessage(response["payload"])
        elif event == "revote":
            await self.revoteMessage(response["payload"])
        elif event == "change_permission":
            await self.changePermissionMessage(response["payload"])

    async def changeUsernameMessage(self, payload):
        if payload["userChannel"] == self.user.channelName:
            await self.changeUsername(payload)
        else:
            await self.changeOtherUsername(payload)

    async def recieveKickUserMessage(self, payload):
        """
        Remove a user from the room if this user has the kick_user permission. Notify the kicked user
        """
        if (not await self.hasPermission(self.user, "kick_user")):
            return

        user = await self.getUserByChannelName(payload["userChannel"])

        if (await self.hasPermission(user, "not_kickable")):
            return

        await self.removeUserFromRoom(user)

        await self.sendMessage(payload["userChannel"], "kicked")

    async def voteMessage(self, payload):
        """
        Stores the vote of an user
        """
        await self.vote(payload["voting"], payload["vote"])

    async def revoteMessage(self, payload):
        """
        Reset votes and broadcast revote to all users
        """
        if (not await self.hasPermission(self.user, "revote")):
            return

        await database_sync_to_async(
            RoomVoting.objects.revote
        )(payload["voting"]["votingId"], self.room)

    async def showVotesMessage(self, payload):
        """
        Broadcast votes to all users
        """
        if (not await self.hasPermission(self.user, "show_votes")):
            return

        await database_sync_to_async(
            RoomVoting.objects.vote
        )(payload["voting"]["votingId"], self.room)

    async def nextVoting(self, payload):
        """
        Ends current voting and tell all user to move to the next voting
        """
        if (not await self.hasPermission(self.user, "next_voting")):
            return

        await database_sync_to_async(
            RoomVoting.objects.votingFinished
        )(payload["voting"]["votingId"], self.room)

    async def votingNameChangedMessage(self, payload):
        """
        Change name of a voting
        """
        if (not await self.hasPermission(self.user, "change_voting_names")):
            return

        voting = payload["voting"]

        await database_sync_to_async(
            RoomVoting.objects.changeName
        )(voting["votingId"], voting["name"], self.room)

    async def addVotingMessage(self):
        """
        Add new voting
        """
        if (not await self.hasPermission(self.user, "add_voting")):
            return

        await database_sync_to_async(RoomVoting.objects.addVoting)(self.room)

    async def removeVotingMessage(self, payload):
        """
        Remove given voting
        """
        if (not await self.hasPermission(self.user, "delete_voting")):
            return

        await database_sync_to_async(
            RoomVoting.objects.removeVoting
        )(payload["voting"]["votingId"], self.room)

    async def changePermissionMessage(self, payload):
        """
        Change permission of specified user
        """
        if (not await self.hasPermission(self.user, "change_permissions")):
            return

        if (not await self.hasPermission(self.user, payload["permissioName"])):
            return

        await self.changePermission(
            payload["userChannel"],
            payload["permissioName"],
            payload["hasPermission"]
        )

    async def sendMessage(self, userChannel, messageType, payload={}):
        """
        Sends a message to the user
        """
        message = {
            "type": messageType, "payload": payload
        }

        await self.channel_layer.send(
            userChannel, {
                "type": "message", "message": json.dumps(message)
            }
        )

    async def broadcastMessage(self, messageType, payload={}):
        """
        Sends a message to all users
        """
        message = {
            "type": messageType, "payload": payload
        }

        await self.channel_layer.group_send(
            self.room.roomChannelName, {
                "type": "message", "message": json.dumps(message)
            }
        )

    @database_sync_to_async
    def changePermission(self, userChannel, permissionName, hasPermission):
        user = RoomUser.objects.getUser(userChannel)
        if hasPermission:
            user.addPermission(permissionName)
        else:
            user.removePermission(permissionName)

        # Send update to user and everyone else
        self.room.sendOwnUserMessage(
            user, False, False
        )  # Overwrite cookie basically means here: This is not a name change
        self.room.broadcastChanged()

    @database_sync_to_async
    def hasPermission(self, user, permission):
        return user.hasPermission(permission)

    @database_sync_to_async
    def getUser(self):
        return self.user.asDict()

    @database_sync_to_async
    def getUserByChannelName(self, channelName):
        return RoomUser.objects.get(channelName=channelName)

    @database_sync_to_async
    def changeUsername(self, payload):
        if self.user.hasPermission("change_username"):
            self.saveUserName(self.user, payload["userName"])

        # Broadcast anway, so the user resets the username if he didn't have the permission to change it
        self.room.broadcastChanged()

    @database_sync_to_async
    def changeOtherUsername(self, payload):
        if self.user.hasPermission("change_other_username"):
            user = RoomUser.objects.getUser(channelName=payload["userChannel"])
            self.saveUserName(user, payload["userName"])

        # Broadcast anway, so the user resets the username if he didn't have the permission to change it
        self.room.broadcastChanged()

    def saveUserName(self, user, newUsername):
        userNames = [
            otherUser["userName"] for otherUser in self.room.getUserPresences()
        ]

        self.logger.error("username: " + newUsername)

        # Reset newUsername to Username without counter
        pattern = " \([0-9]*\)+"
        newUsername = re.sub(pattern, '', newUsername)
        self.logger.error("username without: " + newUsername)

        updateCookie = True
        if (newUsername in userNames):
            # Find next free numbered Username
            counter = 1
            updateCookie = False
            self.logger.error(newUsername + " (" + str(counter) + ")")
            while newUsername + " (" + str(counter) + ")" in userNames:
                counter = counter + 1
                self.logger.error(
                    "Already used " + newUsername + " (" + str(counter) + ")"
                )

            newUsername = newUsername + " (" + str(counter) + ")"

        user.userName = newUsername
        user.save()

        self.room.sendOwnUserMessage(user, updateCookie, True)

    @database_sync_to_async
    def vote(self, voting, vote):
        roomVoting = RoomVoting.objects.get(votingId=voting["votingId"])
        UserVote.objects.vote(self.user, roomVoting, vote)

    # Event Handler
    async def message(self, event):
        """
        Handles generic message event when sent by channel_layer
        """
        await self.send(text_data=event['message'])
