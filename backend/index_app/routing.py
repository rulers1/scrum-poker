from django.conf.urls import url
from .consumers import CommunicationConsumer

websocket_urlpatterns = [
    url(r'^ws/(?P<room_team>\w+)/(?P<room_code>\w+)/$', CommunicationConsumer.as_asgi()),
]
