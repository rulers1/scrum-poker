from django.apps import AppConfig


class IndexAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'index_app'

    # Test Code, database calls in ready should be avoided actually
    # def ready(self):
    #     from .models import Counter

    #     counter = Counter(counter=0)
    #     counter.save()
