from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('room/<roomCode>', views.room, name='room'),
    path('team/<roomCode>', views.team, name='room'),
    path('team/', views.team_room_overview, name='team_room_overview'),
    path('room/', views.room_overview, name='room_overview'),
]
