from django.shortcuts import render, redirect


def index(request):
    if request.method == "POST":

        roomCode = request.POST.get("room_code")
        print(roomCode)

        if not roomCode:
            print("No Room Code")
            return render(request, "index.html")

        createRequest = request.POST.get("room_team")
        print(createRequest)

        if createRequest == "room":
            return redirect(f"/room/{roomCode}")
        elif createRequest == "team":
            return redirect(f"/team/{roomCode}")

    return render(request, "index.html")


def room_overview(request):
    if request.method == "POST":
        if request.POST.get("room_code"):
            roomCode = request.POST.get("room_code")
            return redirect(f"/room/{roomCode}")
    return render(request, "room_overview.html")


def team_room_overview(request):
    if request.method == "POST":
        if request.POST.get("room_code"):
            roomCode = request.POST.get("room_code")
            return redirect(f"/team/{roomCode}")
    return render(request, "team_room_overview.html")


def room(request, roomCode):
    return render(
        request, "room.html", {
            'roomCode': roomCode, 'room_team': "room"
        }
    )


def team(request, roomCode):
    return render(
        request, "room.html", {
            'roomCode': roomCode, 'room_team': "team"
        }
    )
