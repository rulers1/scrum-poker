"""
This file is based on https://github.com/mitmedialab/django-channels-presence provided under MIT License.
"""

import json
from datetime import timedelta
from warnings import catch_warnings
from django.conf import settings

from django.db import models, transaction
from django.utils.timezone import now

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

channel_layer = get_channel_layer()


class RoomManager(models.Manager):
    def get(self, roomCode, roomChannelName, persistent):
        return Room.objects.get_or_create(
            roomCode=roomCode,
            roomChannelName=roomChannelName,
            persistent=persistent
        )[0]


class Room(models.Model):
    roomChannelName = models.CharField(
        max_length=255,
        unique=True,
        help_text="Group channel name for this room"
    )

    roomCode = models.CharField(max_length=255)

    persistent = models.BooleanField(default=False)

    objects = RoomManager()

    def __str__(self):
        return self.roomChannelName

    @transaction.atomic(durable=True)
    def addPresence(self, user):
        """
        Add new presence to room or set existing presence to online
        """
        from .signals import presenceAdded

        presence, created = Presence.objects.get_or_create(room=self, user=user)
        presence.setstatus(True)
        async_to_sync(channel_layer.group_add
                      )(self.roomChannelName, user.channelName)

        presenceAdded.send(sender=self.__class__, room=self, added=presence)

    def disconnectPresence(self, user):
        """
        Disconnect presence from room if it exists.
        """
        presence = Presence.objects.get(room=self, user=user)
        presence.setstatus(False)
        if not (self.persistent and user.user):
            self.removePresence(user)
        else:
            async_to_sync(channel_layer.group_discard
                          )(self.roomChannelName, user.channelName)
            self.broadcastChanged()

    def removePresence(self, user):
        """
        Remove presence from room if it exists.
        """
        from .signals import presenceRemoved

        try:
            presence = Presence.objects.get(room=self, user=user)
        except Presence.DoesNotExist:
            return
        presence.setstatus(False)

        async_to_sync(channel_layer.group_discard
                      )(self.roomChannelName, user.channelName)
        presence.delete()
        if not Presence.objects.filter(room=self).exists():
            self.delete()
        presenceRemoved.send(
            sender=self.__class__, room=self, removed=presence
        )
        self.broadcastChanged()

    def getUserPresences(self):
        return [presence.user.asDict() for presence in self.presence_set.all()]

    def getUser(self):
        return [presence.user for presence in self.presence_set.all()]

    def atLeastOneAdmin(self):
        """
        Checks if room has at least one admin
        """
        for presence in self.presence_set.all():
            if presence.user.hasGroup("admin"):
                return True
        return False

    def broadcastChanged(self):
        from .signals import presenceChanged

        presenceChanged.send(sender=self.__class__, room=self)

    def sendOwnUserMessage(
        self, user, changeCookie, checkCookieCondition=True
    ):
        from .signals import ownPresenceChanged

        ownPresenceChanged.send(
            sender=self.__class__,
            user=user,
            changeCookie=changeCookie,
            checkCookieCondition=checkCookieCondition
        )

    @transaction.atomic(durable=True)
    def getUserByLoginAndUpdate(self, user, channelName, persistent):
        if persistent:
            for presence in self.presence_set.all():
                if presence.user.user == user:
                    editpresence = Presence.objects.get(
                        room=self, user=presence.user
                    )
                    editpresence.setChannelName(channelName)
                    return editpresence.user
            return None
        return None


class Permission(models.Model):
    name = models.CharField(unique=True, max_length=100)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Group(models.Model):
    name = models.CharField(
        max_length=255, unique=True, help_text='Group Name'
    )

    permissions = models.ManyToManyField(
        Permission, blank=True, help_text='List of room permissions'
    )

    def hasPermission(self, name):
        return any([
            permission.name == name for permission in self.permissions.all()
        ])

    def addPermission(self, name):
        try:
            permission = Permission.objects.get(name=name)
        except Permission.DoesNotExist:
            print(f"Warning: Permission {name} does not exist")
            return
        self.permissions.add(permission)
        self.save()

    def removePermission(self, name):
        try:
            permission = Permission.objects.get(name=name)
        except Permission.DoesNotExist:
            print(f"Warning: Permission {name} does not exist")
            return

        self.permissions.remove(permission)
        self.save()

    def getPermissions(self):
        return list(self.permissions.all())

    def __str__(self):
        return self.name


class RoomUserManager(models.Manager):
    """
    Manager for RoomUser
    """
    def createUser(self, channelName, userName, user=None):
        """
        Creates a new RoomUser
        """
        if not user or not user.is_authenticated:
            user = None
        else:
            userName = user.username
        group = Group.objects.create(name=channelName)
        return self.create(
            channelName=channelName,
            userName=userName,
            permissionGroup=group,
            user=user
        )

    def getUser(self, channelName):
        """
        Get user by channel name
        """
        return self.get(channelName=channelName)


class RoomUser(models.Model):
    """
    A custom user model for rooms
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE
    )

    channelName = models.CharField(
        max_length=255,
        help_text="Reply channel for connection that is present"
    )

    userName = models.CharField(
        max_length=255, help_text="Free choosen user name"
    )

    groups = models.ManyToManyField(
        Group,
        blank=True,
        help_text="List of groups that grant user permissions of these"
    )

    permissionGroup = models.ForeignKey(
        Group,
        related_name="group",
        help_text="Custom permission group",
        on_delete=models.CASCADE
    )

    objects = RoomUserManager()

    def __str__(self):
        return self.channelName

    def changeUserName(self, newUserName):
        self.userName = newUserName
        self.save()

    def setChannelName(self, newChannelName):
        self.channelName = newChannelName
        self.save()

    def addGroup(self, name):
        try:
            group = Group.objects.get(name=name)
        except Group.DoesNotExist:
            print(f"Warning: Group {name} does not exist")
            return
        self.groups.add(group)
        self.save()

    def removeGroup(self, name):
        try:
            group = Group.objects.get(name=name)
        except Group.DoesNotExist:
            print(f"Warning: Group {name} does not exist")
            return
        self.groups.remove(group)
        self.save()

    def hasGroup(self, name):
        """
        Checks if user has given group
        """
        try:
            self.groups.get(name=name)
            return True
        except Group.DoesNotExist:
            return False

    def addPermission(self, name):
        self.permissionGroup.addPermission(name)
        self.save()

    def removePermission(self, name):
        for group in self.groups.all():
            if group.hasPermission(name):
                self.copyGroupToPermissionGroup(group)

        self.permissionGroup.removePermission(name)
        self.save()

    def copyGroupToPermissionGroup(self, group):
        for permission in group.permissions.all():
            self.addPermission(permission)
        self.groups.remove(group)
        self.save()

    def hasPermission(self, name):
        return any([self.permissionGroup.hasPermission(name)] +
                   [group.hasPermission(name) for group in self.groups.all()])

    def getPermissions(self):
        permissions = self.permissionGroup.getPermissions()
        for group in self.groups.all():
            permissions += group.getPermissions()
        return [str(permission) for permission in permissions]

    def getGroups(self):
        return [str(group) for group in self.groups.all()]

    def getStatus(self):
        Presence.objects.get(room=self)

    def asDict(self):
        return {
            "userName": self.userName,
            "userChannelName": self.channelName,
            "permissions": self.getPermissions(),
            "groups": self.getGroups(),
            "status": self.presence.status
        }


class Presence(models.Model):
    """
    Active user session associated with a room.

    Automatically deletes the associated user and room.
    """
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    user = models.OneToOneField(
        RoomUser, on_delete=models.CASCADE, related_name="presence"
    )

    lastSeen = models.DateTimeField(default=now)

    status = models.BooleanField(default=False)

    def setChannelName(self, newChannelName):
        self.user.setChannelName(newChannelName)
        self.save()

    def setstatus(self, online):
        self.status = online
        self.save()

    def __str__(self):
        return str(self.user)


class RoomVotingManager(models.Manager):
    def getVotings(self, room):
        return [{
            "votingId": voting.votingId,
            "name": voting.name,
            "voted": voting.voted,
            "done": voting.done,
            "votes": self.getVotes(voting)
        } for voting in self.filter(room=room).all()]

    def getVotes(self, voting):
        if not voting.voted:
            return []

        return {
            userVote.user.userName: userVote.vote
            for userVote in voting.userVotes.all()
        }

    def changeName(self, votingId, votingName, room):
        self.update_or_create(
            votingId=votingId, defaults={"name": votingName}
        )
        self.votingChanged(room)

    def addVoting(self, room):
        self.create(room=room)
        self.votingChanged(room)

    def vote(self, votingId, room):
        """
        Sets voted to true
        """
        voting = self.get(votingId=votingId)
        voting.voted = True
        voting.save()
        self.votingChanged(room)

    def revote(self, votingId, room):
        """
        Sets voted to false and deletes all votes
        """
        voting = self.get(votingId=votingId)
        voting.voted = False
        voting.done = False
        voting.save()
        self.votingChanged(room)

    def votingFinished(self, votingId, room):
        """
        Set vote done
        """
        voting = self.get(votingId=votingId)
        voting.done = True
        voting.save()
        self.votingChanged(room)

    def addDefaultVoting(self, room):
        if not self.filter(room=room).exists():
            self.addVoting(room)

    def removeVoting(self, votingId, room):
        self.get(votingId=votingId).delete()
        self.votingChanged(room)
        self.addDefaultVoting(room)

    def votingChanged(self, room):
        from .signals import votingChanged

        votingChanged.send(
            sender=self.__class__, room=room, votings=self.getVotings(room)
        )


class RoomVoting(models.Model):
    votingId = models.AutoField(primary_key=True)

    name = models.CharField(max_length=255, help_text="Optional Voting name")

    room = models.ForeignKey(
        Room,
        on_delete=models.CASCADE,
        help_text="Room associated with voting"
    )

    voted = models.BooleanField(
        default=False,
        help_text="Determines whether all voted / voting was uncovered"
    )

    done = models.BooleanField(
        default=False,
        help_text="Determines whether vote is done and archieved"
    )

    objects = RoomVotingManager()


class UserVoteManager(models.Manager):
    """
    Manager for Votes
    """
    def vote(self, user, voting, vote):
        """
        Updates or creates a vote for the given user and voting
        """
        self.update_or_create(
            user=user,
            voting=voting,
            defaults={'vote': vote},
        )

    def clearVotes(self):
        self.all().delete()


class UserVote(models.Model):
    user = models.ForeignKey(
        RoomUser,
        on_delete=models.CASCADE,
        help_text="User associated with this vote"
    )
    voting = models.ForeignKey(
        RoomVoting, related_name='userVotes', on_delete=models.CASCADE
    )

    vote = models.CharField(max_length=10, help_text="Vote of user in a room")

    objects = UserVoteManager()

    class Meta:
        unique_together = (('user', 'voting'), )
