module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-recommended",
    "plugin:prettier-vue/recommended",
    "prettier",
  ],
  env: {
    node: true,
  },

  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "off" : "warn",
    "no-debugger": process.env.NODE_ENV === "production" ? "off" : "warn",
    "prettier-vue/prettier": [
      "error",
      {
        endOfLine: "lf",
      },
    ],
  },
};
