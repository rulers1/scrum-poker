const BundleTracker = require("webpack-bundle-tracker");
const path = require("path");

const pages = {
  index: {
    entry: "./src/index.js",
    chunks: ["chunk-vendors"],
  },
  login: {
    entry: "./src/login.js",
    chunks: ["chunk-vendors"],
  },
  team_room_overview: {
    entry: "./src/team_room_overview.js",
    chunks: ["chunk-vendors"],
  },
  lastseenroomoverview: {
    entry: "./src/lastseenroomoverview.js",
    chunks: ["chunk-vendors"],
  },
  room: {
    entry: "./src/room.js",
    chunks: ["chunk-vendors"],
  },
  profile: {
    entry: "./src/profile.js",
    chunks: ["chunk-vendors"],
  },
};

module.exports = {
  configureWebpack: {
    devtool: "source-map",
  },
  pages: pages,
  filenameHashing: false,
  productionSourceMap: false,
  publicPath: "/static/vue",
  outputDir: "../backend/static/vue/",

  pluginOptions: {
    devtools: process.env.NODE_ENV !== "production",

    i18n: {
      locale: "de",
      fallbackLocale: "en",
      localeDir: "@/locales",
      enableLegacy: false,
      runtimeOnly: false,
      fullInstall: true,
    },
  },

  chainWebpack: (config) => {
    config.module
      .rule("i18n-resource")
      .test(/\.(json5?|ya?ml)$/)
      .include.add(path.resolve(__dirname, "./locales"))
      .end()
      .type("javascript/auto")
      .use("i18n-resource")
      .loader("@intlify/vue-i18n-loader");

    config.module
      .rule("i18n")
      .resourceQuery(/blockType=i18n/)
      .type("javascript/auto")
      .use("i18n")
      .loader("@intlify/vue-i18n-loader");

    config.optimization.splitChunks({
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "chunk-vendors",
          chunks: "all",
          priority: 1,
        },
      },
    });

    Object.keys(pages).forEach((page) => {
      config.plugins.delete(`html-${page}`);
      config.plugins.delete(`preload-${page}`);
      config.plugins.delete(`prefetch-${page}`);
    });

    config
      .plugin("BundleTracker")
      .use(BundleTracker, [{ filename: "webpack-stats.json" }]);

    config.resolve.alias.set("__STATIC__", "static");

    config.devServer
      .public("http://localhost:8080")
      .host("localhost")
      .port(8080)
      .hotOnly(true)
      .watchOptions({ poll: 1000 })
      .https(false)
      .headers({ "Access-Control-Allow-Origin": ["*"] });
  },
};
