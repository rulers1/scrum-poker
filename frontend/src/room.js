import { createApp } from "vue";
import RoomApp from "./RoomApp.vue";
import i18n from "@/i18n";

import { VueCookieNext } from "vue-cookie-next";

import "../css/bulma.css";
import "./localstorage.js";

createApp(RoomApp).use(i18n).use(VueCookieNext).mount("#app");
