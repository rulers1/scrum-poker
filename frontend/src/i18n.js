import { createI18n } from "vue-i18n";

export default createI18n({
  locale: "de", // set locale
  fallbackLocale: "en", // set fallback locale
  legacy: false,
});
