export function getFromLocalStorage(key) {
  let list = [];
  if (localStorage.getItem(key)) {
    try {
      return JSON.parse(localStorage.getItem(key));
    } catch (e) {
      console.warn(e);
      localStorage.removeItem(key);
    }
    return list;
  }
}

export function setToLocalStorage(key, data) {
  localStorage.setItem(key, JSON.stringify(data));
}
