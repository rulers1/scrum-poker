import { createApp } from "vue";
import HomeApp from "./HomeApp.vue";
import i18n from "./i18n.js";

import "../css/bulma.css";

createApp(HomeApp).use(i18n).mount("#app");
