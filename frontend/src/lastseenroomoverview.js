import { createApp } from "vue";
import LastSeenRoomOverview from "./LastSeenRoomOverview.vue";
import i18n from "@/i18n";

import "../css/bulma.css";

createApp(LastSeenRoomOverview).use(i18n).mount("#app");
