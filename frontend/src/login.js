import { createApp } from "vue";
import LginApp from "./LoginApp.vue";
import i18n from "@/i18n";

import "../css/bulma.css";

createApp(LginApp).use(i18n).mount("#app");
