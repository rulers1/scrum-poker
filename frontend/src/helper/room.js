import { getFromLocalStorage } from "@/localstorage";

export function getLastRooms() {
  let lastRooms = getFromLocalStorage("lastrooms") || [];
  return lastRooms.slice(0, 15).map((roomName) => {
    var room = {};
    room["name"] = roomName;
    return room;
  });
}
