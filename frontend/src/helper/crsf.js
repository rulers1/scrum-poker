export function getCrsfToken() {
  const tokens = document.getElementsByName("csrfmiddlewaretoken");
  return tokens[0].defaultValue;
}
