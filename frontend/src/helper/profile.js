export function getDisplayName() {
  return JSON.parse(document.getElementById("account-username").textContent);
}

export function getSocialAccountProfile() {
  let socialAccountList = JSON.parse(
    document.getElementById("socialaccount_list").textContent
  );

  if (Object.keys(socialAccountList).length === 0) return null;

  // Currently only one social account supported in frontend
  return socialAccountList[Object.keys(socialAccountList)][0];
}

export function getTeams() {
  return JSON.parse(document.getElementById("teams").textContent);
}
