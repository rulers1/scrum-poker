export async function postData(url = "", csrf, data = {}) {
  const response = await fetch(url, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrf,
    },
    redirect: "manual",
    referrerPolicy: "no-referrer",
    body: JSON.stringify(data),
  });
  return response;
}
