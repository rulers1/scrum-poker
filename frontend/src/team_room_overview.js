import { createApp } from "vue";
import TeamRoomOverview from "./TeamRoomOverview.vue";
import i18n from "@/i18n";

import "../css/bulma.css";

createApp(TeamRoomOverview).use(i18n).mount("#app");
