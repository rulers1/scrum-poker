import { createApp } from "vue";
import ProfileApp from "./ProfileApp.vue";
import i18n from "@/i18n";

import "../css/bulma.css";

createApp(ProfileApp).use(i18n).mount("#app");
