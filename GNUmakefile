.POSIX:
.SUFFIXES:

# Configuration
APP_LIST=index_app login_app
BUILD_JS_DIR = backend/static/vue/js/
BUILD_CSS_DIR = backend/static/vue/css/
DOCKERFILE = Dockerfile

PROJECT_NAME = scrum_poker

# Commands

NPM = \
	npm \
	--prefix frontend

run_manage_py = \
		poetry \
		run \
		python \
		backend/manage.py

DOCKER ?= docker

usage:
	@echo "run_frontend: Run the frontend server"
	@echo "run_frontend_dev: Run the frontend server for development"
	@echo "run_backend: Run the backend server"
	@echo "Please note: For development purposes both need to be running"
	@echo "makemigrations: Create the needed migrations after updating models.py"
	@echo "install_dependencies: Catchall to install all dependencies"
	@echo "install_frontend_dependencies: Install npm dependencies"
	@echo "install_frontend_dependencies_dev: Install npm dependencies for development"
	@echo "install_backend_dependencies: Install poetry/python dependencies"
	@echo "drop_database: Delete database and all migrations"

$(BUILD_CSS_DIR) $(BUILD_JS_DIR): frontend/css frontend/src
	$(NPM) \
		run \
		build

build_css frontend/css: frontend/sass
	$(NPM) \
		run \
		css-build

run_backend: migrate $(BUILD_CSS_DIR) $(BUILD_JS_DIR)
	${run_manage_py} \
		runserver

run_backend_dev: migrate
	$(run_manage_py) \
		my_runserver \
		--webpack-command '$(NPM) run watch'

install_backend_dependencies:
	poetry \
		install

install_frontend_dependencies_dev:
	rm -rf frontend/node_modules
	$(NPM) \
		clean-install \
		--include=dev

install_frontend_dependencies:
	$(NPM) \
		install

install_dependencies: install_frontend_dependencies install_backend_dependencies

makemigrations: $(foreach app,$(APP_LIST),backend/$(app)/models.py)
	${run_manage_py} \
		makemigrations \
		$(app)

INTERMEDIATE: migrate
MIGRATIONS = $(filter-out %/migrations/__init__.py,$(wildcard backend/*/migrations/*.py))
migrate: makemigrations $(MIGRATIONS)
	${run_manage_py} \
		migrate

INTERMEDIATE: %.rm
%.rm: %
	$(run_manage_py) \
		remove_file "$<"

delete_migrations: $(addsuffix .rm,$(MIGRATIONS))
	@echo "Removing $^"

drop_database: delete_migrations
	$(run_manage_py) \
		drop_database

create_database: migrate $(wildcard backend/*/fixtures/permissions.yaml)
	${run_manage_py} \
		loaddata \
		permissions.yaml

recreate_database: drop_database create_database

build_docker: build_docker_app build_docker_nginx

build_docker_app: $(DOCKERFILE)
	$(DOCKER) build \
		--file $(DOCKERFILE) \
		--target run_image \
		--tag "$(PROJECT_NAME)/app" \
		$(PWD)

build_docker_nginx: $(DOCKERFILE)
	$(DOCKER) build \
		--file $(DOCKERFILE) \
		--target nginx_image \
		--tag "$(PROJECT_NAME)/nginx" \
		$(PWD)
